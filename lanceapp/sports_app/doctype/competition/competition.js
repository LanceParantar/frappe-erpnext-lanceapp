// Copyright (c) 2021,   and contributors
// For license information, please see license.txt

frappe.ui.form.on('Competition', {
	 refresh: function(frm) {
		 frm.add_custom_button(__('Get Athlete'), function(){
        		frappe.msgprint(frm.doc.name);
    		 }, __("Action"));
	 }
});
