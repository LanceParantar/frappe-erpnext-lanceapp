# PYTHON API FOR GETTING ATHLETES BASED ON THE SELECTED SPORT

import frappe


@frappe.whitelist()
def get_athletes(sport=None):
	return frappe.db.sql(f"""SELECT full_name FROM tabAthlete;""", as_dict=False)
	