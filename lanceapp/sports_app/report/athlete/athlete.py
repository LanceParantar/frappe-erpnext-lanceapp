# Copyright (c) 2013,   and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import datetime
import frappe

def execute(filters=None):
	columns, data = get_columns(), get_data(filters)
	return columns, data


def get_columns():
	return ['First Name:Data:160','Middle Name:Data:160','Last Name:Data:160', 'Sports:Data:160', 'Start Date:Date:160', 'End Date:Date:160', 'No of Games:Int:140']


def get_data(filters):
	# conditions
	conditions = ""
	if (filters.get('first_name')):conditions += f" where tabAthlete.first_name like '%{filters.get('first_name')}%'"
	if (filters.get('middle_name')):conditions += f" where tabAthlete.middle_name like '%{filters.get('middle_name')}%'"
	if (filters.get('last_name')):conditions += f" where tabAthlete.last_name like '%{filters.get('last_name')}%'"
	if (filters.get('sports')):conditions += f" where tabGames.sport like '%{filters.get('sports')}%'"

	data = frappe.db.sql(f"""select tabAthlete.first_name, tabAthlete.middle_name, tabAthlete.last_name, tabGames.sport, tabGames.start_date, tabGames.end_date, tabGames.no_of_games
			from `tabAthlete` left join `tabGames` on (`tabGames`.parent = `tabAthlete`.name) {conditions} ;""")
	
	return data
