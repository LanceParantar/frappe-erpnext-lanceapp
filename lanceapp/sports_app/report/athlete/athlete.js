// Copyright (c) 2016,   and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["Athlete"] = {
	"filters": [
		{
			"fieldname" : "first_name",
			"label" : __("First Name"),
			"fieldtype" : "Data",
			"width" : 100,
			"reqd" : 0,
		},
		{
			"fieldname" : "middle_name",
			"label" : __("Middle Name"),
			"fieldtype" : "Data",
			"width" : 100,
			"reqd" : 0,
		},
		{
			"fieldname" : "last_name",
			"label" : __("Last Name"),
			"fieldtype" : "Data",
			"width" : 100,
			"reqd" : 0,
		},
		{
			"fieldname" : "sports",
			"label" : __("Sport Name"),
			"fieldtype" : "Data",
			"width" : 100,
			"reqd" : 0,
		}
	]
};
